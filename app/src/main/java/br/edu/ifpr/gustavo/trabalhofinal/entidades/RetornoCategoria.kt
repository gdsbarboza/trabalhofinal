package br.edu.ifpr.gustavo.trabalhofinal.entidades

import androidx.room.Entity

data class RetornoCategoria(
    var trivia_categories: List<Categoria>
)