package br.edu.ifpr.gustavo.trabalhofinal.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpr.gustavo.trabalhofinal.R
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Categoria
import kotlinx.android.synthetic.main.card_categoria.view.*

class CategoriaAdapter (var categoria: List<Categoria>, private var listener: CategoriaListListener) :
RecyclerView.Adapter<CategoriaAdapter.CategoriaViewHolder>() {

    override fun getItemCount() = categoria.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoriaViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.card_categoria, parent, false)
        )

    override fun onBindViewHolder(holder: CategoriaViewHolder, position: Int) {
        holder.fillUI(categoria[position])
    }


    inner class CategoriaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun fillUI(categoria: Categoria) {

            itemView.btEasy.visibility = View.GONE
            itemView.btMedium.visibility = View.GONE
            itemView.btHard.visibility = View.GONE

            itemView.txtCategoria.text = categoria.name
            itemView.setOnClickListener {

                if (itemView.btEasy.visibility != View.VISIBLE || itemView.btMedium.visibility != View.VISIBLE || itemView.btHard.visibility != View.VISIBLE) {
                    itemView.btEasy.visibility = View.VISIBLE
                    itemView.btMedium.visibility = View.VISIBLE
                    itemView.btHard.visibility = View.VISIBLE
                }else{
                    itemView.btEasy.visibility = View.GONE
                    itemView.btMedium.visibility = View.GONE
                    itemView.btHard.visibility = View.GONE
                }
                itemView.btEasy.setOnClickListener {
                    listener.abrirActivity(categoria, "easy")
                }
                itemView.btMedium.setOnClickListener {
                    listener.abrirActivity(categoria, "medium")
                }
                itemView.btHard.setOnClickListener {
                    listener.abrirActivity(categoria, "hard")
                }
            }

        }


    }

}
