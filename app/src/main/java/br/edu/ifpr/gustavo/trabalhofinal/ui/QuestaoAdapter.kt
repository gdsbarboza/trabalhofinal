package br.edu.ifpr.gustavo.trabalhofinal.ui

import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpr.gustavo.trabalhofinal.R
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Pergunta
import kotlinx.android.synthetic.main.item_questao_local.view.*

class QuestaoAdapter(var resultado: List<Pergunta>, private var listener: QuestaoListListener) :
    RecyclerView.Adapter<QuestaoAdapter.QuestaoViewHolder>() {
    override fun getItemCount() = resultado.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        QuestaoViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_questao_local, parent, false)
        )

    override fun onBindViewHolder(holder: QuestaoViewHolder, position: Int) {
        holder.fillUI(resultado[position])
    }

    inner class QuestaoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fillUI(pergunta: Pergunta) {


            itemView.txtQuestaoPergunta.text = if (Build.VERSION.SDK_INT >= 24)
                Html.fromHtml(pergunta.questao, Html.FROM_HTML_MODE_LEGACY).toString()
            else
                Html.fromHtml(pergunta.questao).toString()

            itemView.setOnClickListener {
                listener.abrirActivity(pergunta)
            }

        }
    }

}

