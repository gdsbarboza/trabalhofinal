package br.edu.ifpr.gustavo.trabalhofinal.entidades

import java.io.Serializable

class RespostaIncorreta(
    var resp1: String,
    var resp2: String,
    var resp3: String
) : Serializable