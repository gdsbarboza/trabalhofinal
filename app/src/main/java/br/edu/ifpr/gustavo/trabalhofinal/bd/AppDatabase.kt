package br.edu.ifpr.gustavo.trabalhofinal.bd

import androidx.room.Database
import androidx.room.RoomDatabase
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Pergunta
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Usuario


@Database(entities = arrayOf(Pergunta::class), version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun resultadoDao(): ResultadoDao
}
