package br.edu.ifpr.gustavo.trabalhofinal.service

import br.edu.ifpr.gustavo.trabalhofinal.entidades.*
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.*

interface UsuarioService {

    @Headers("Accept: application/json")
    @FormUrlEncoded

    @POST("usuario/login")
    fun fazerLogin(

        @Field("email")
        email: String,

        @Field("senha")
        senha: String

    ): Call<RetornoLogin>

    @POST("usuario/registrar")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    fun criarUsuario(
        @Field("nome") nome: String,
        @Field("email") email: String,
        @Field("senha") senha: String
        ): Call<RetornoCriarUsuario>

    @Headers("Accept: application/json")
    @PUT("usuario/pontuacao")
    @FormUrlEncoded
    fun marcarPontuacao(

        @Field("email") email: String,
        @Field("senha") senha: String,
        @Field("pontos") pontos: Int

    ): Call<RetornoPontuacao>

    @Headers("Accept: application/json")

    @GET("ranking")
    fun mostrarRanking(): Call<RetornoRanking>

}