package br.edu.ifpr.gustavo.trabalhofinal.entidades

import java.io.Serializable

data class Categoria (
    var id: Int,
    var name: String
) : Serializable