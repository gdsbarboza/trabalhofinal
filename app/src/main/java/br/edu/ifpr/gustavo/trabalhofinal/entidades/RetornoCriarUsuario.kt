package br.edu.ifpr.gustavo.trabalhofinal.entidades

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "ranking")
data class RetornoCriarUsuario (

    var sucesso: Boolean,
    var mensagem: String

)