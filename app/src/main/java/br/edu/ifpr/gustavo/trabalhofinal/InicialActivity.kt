package br.edu.ifpr.gustavo.trabalhofinal

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_inicial.*

class InicialActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_inicial)

		remoteBt.setOnClickListener {
			val intentGenerica = Intent(this, CategoriasActivity::class.java)
			startActivity(intentGenerica)
		}
		localBt.setOnClickListener {
			val intentGenerica = Intent(this, LocalActivity::class.java)
			startActivity(intentGenerica)
		}
		rankingBt.setOnClickListener {
			val intentGenerica = Intent(this, RankingMain::class.java)
			startActivity(intentGenerica)
		}

	}
}