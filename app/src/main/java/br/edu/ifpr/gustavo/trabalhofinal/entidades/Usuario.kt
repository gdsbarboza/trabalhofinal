package br.edu.ifpr.gustavo.trabalhofinal.entidades

import java.util.*

data class Usuario(
    var nome: String,
    var pontuacao: Int,
    var partidasJogadas: Int,
    var ultimaPartida: Date
)