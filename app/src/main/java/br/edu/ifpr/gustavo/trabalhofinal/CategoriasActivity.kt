package br.edu.ifpr.gustavo.trabalhofinal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Categoria
import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoCategoria
import br.edu.ifpr.gustavo.trabalhofinal.service.JogoService
import br.edu.ifpr.gustavo.trabalhofinal.ui.CategoriaAdapter
import kotlinx.android.synthetic.main.activity_categorias.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpr.gustavo.trabalhofinal.ui.CategoriaListListener
import kotlin.random.Random


class CategoriasActivity : AppCompatActivity(), CategoriaListListener {

	override fun abrirActivity(categoria: Categoria, dificuldade: String) {
		val intentExplicita = Intent(this, JogoActivity::class.java)
		intentExplicita.putExtra("dificuldade", dificuldade)
		intentExplicita.putExtra("Categoria", categoria)
		startActivity(intentExplicita)
	}

	lateinit var retrofit: Retrofit
	lateinit var service: JogoService
	lateinit var adapter: CategoriaAdapter

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_categorias)

		configuraRetrofit()
		service.coletaCategoria().enqueue(object : Callback<RetornoCategoria> {
			override fun onFailure(call: Call<RetornoCategoria>, t: Throwable) {
			}
			override fun onResponse(call: Call<RetornoCategoria>, response: Response<RetornoCategoria>) {
				val r = response.body()!!

				randomButton.setOnClickListener{
					val p = Random.nextInt(0,r.trivia_categories.size)
					val x = Random.nextInt(0,2)

					if(x == 0)
						abrirActivity(r.trivia_categories.get(p), "easy")
					if(x == 1)
						abrirActivity(r.trivia_categories.get(p), "medium")
					if(x == 2)
						abrirActivity(r.trivia_categories.get(p), "hard")
				}

				loadRecyclerView(r)
			}
		})
	}

	fun configuraRetrofit() {
		retrofit = Retrofit.Builder()
				.baseUrl("https://opentdb.com/")
				.addConverterFactory(GsonConverterFactory.create())
				.build()
		service = retrofit.create(JogoService::class.java)
	}

	private fun loadRecyclerView(categorias: RetornoCategoria) {
		if (listView != null) {
			val mRecycler = this.findViewById(R.id.listView) as RecyclerView
			mRecycler.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)
			adapter = CategoriaAdapter(categorias.trivia_categories, this)
			mRecycler.adapter = adapter
		}
	}

}
