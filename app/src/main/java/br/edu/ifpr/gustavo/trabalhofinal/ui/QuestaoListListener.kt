package br.edu.ifpr.gustavo.trabalhofinal.ui

import br.edu.ifpr.gustavo.trabalhofinal.entidades.Pergunta

interface QuestaoListListener {
    fun abrirActivity(pergunta: Pergunta)
}