package br.edu.ifpr.gustavo.trabalhofinal

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import br.edu.ifpr.gustavo.trabalhofinal.bd.AppDatabase
import br.edu.ifpr.gustavo.trabalhofinal.bd.ResultadoDao
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Categoria
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Pergunta
import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoPergunta
import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoPontuacao
import br.edu.ifpr.gustavo.trabalhofinal.service.JogoService
import br.edu.ifpr.gustavo.trabalhofinal.service.UsuarioService
import kotlinx.android.synthetic.main.activity_jogo.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class JogoActivity() : AppCompatActivity() {
    lateinit var retrofit: Retrofit
    lateinit var service: JogoService
    lateinit var retrofit2: Retrofit
    lateinit var serviceUsuario: UsuarioService
    lateinit var dificuldade: String
    lateinit var timer: CountDownTimer
    var timerFinished: Boolean = false
    lateinit var db: AppDatabase
    lateinit var perguntaDao: ResultadoDao
    lateinit var b: Categoria
    lateinit var c: Pergunta

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jogo)

        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "perguntas.db"
        )
            .allowMainThreadQueries()
            .addMigrations()
            .build()
        perguntaDao = db.resultadoDao()

        if (getIntent().hasExtra("dificuldade"))
            dificuldade = intent.getSerializableExtra("dificuldade") as String
        if (getIntent().hasExtra("Categoria"))
            b = getIntent().getSerializableExtra("Categoria") as Categoria
        if (getIntent().hasExtra("Pergunta")) {
            c = getIntent().getSerializableExtra("Pergunta") as Pergunta
        }
        configuraRetrofit()
        configuraRetrofitUsuario()
        if (::c.isInitialized) {
            loadQuestSimple(c)
        }




        if (::b.isInitialized && !::c.isInitialized) {

            service.coletaQuestao(1, b.id, dificuldade).enqueue(object : Callback<RetornoPergunta> {
                override fun onFailure(call: Call<RetornoPergunta>, t: Throwable) {
                }

                override fun onResponse(call: Call<RetornoPergunta>, response: Response<RetornoPergunta>) {
                    val r = response.body()!!
                    loadQuest(r.perguntas)
                }
            })
        }
        btLocal.setOnClickListener {
            val intentExplicita = Intent(this, LocalActivity::class.java)
            intentExplicita.putExtra("dificuldade", dificuldade)
            intentExplicita.putExtra("Categoria", b)
            startActivity(intentExplicita)


        }
        btNextQuest.setOnClickListener {
            timer.cancel()
            service.coletaQuestao(1, b.id, dificuldade).enqueue(object : Callback<RetornoPergunta> {
                override fun onFailure(call: Call<RetornoPergunta>, t: Throwable) {
                }

                override fun onResponse(call: Call<RetornoPergunta>, response: Response<RetornoPergunta>) {
                    val r = response.body()!!
                    loadQuest(r.perguntas)
                }
            })

        }
        btRanking.setOnClickListener {
            timer.cancel()
            val intentExplicita = Intent(this, RankingMain::class.java)
            startActivity(intentExplicita)
        }
    }

    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
            .baseUrl("https://opentdb.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(JogoService::class.java)
    }

    fun configuraRetrofitUsuario() {
        retrofit2 = Retrofit.Builder()
            .baseUrl("https://tads2019-todo-list.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        serviceUsuario = retrofit2.create(UsuarioService::class.java)
    }

    private fun loadQuestSimple(pergunta: Pergunta) {
        btPerguntaTardia.visibility = View.GONE
        if (Build.VERSION.SDK_INT >= 24)
            txtQuestao.text = Html.fromHtml(pergunta.questao, Html.FROM_HTML_MODE_LEGACY).toString()
        else
            txtQuestao.text = Html.fromHtml(pergunta.questao).toString()

        if (pergunta.tipo == "multiple") {
            val random = Random()
            pergunta.respostas_incorretas += pergunta.resposta_certa

            var lista = pergunta.respostas_incorretas.shuffled()
            lista.drop(
                3
            )
            if (lista.lastIndex == 5) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.text = lista[3]
            } else if (lista.lastIndex == 4) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.text = lista[3]
            } else if (lista.lastIndex == 3) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.text = lista[3]
            } else if (lista.lastIndex == 2) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.visibility = View.GONE
            } else if (lista.lastIndex == 1) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.visibility = View.GONE
                btResp4.visibility = View.GONE
            } else {
                btResp.text = lista[0]
                btResp2.visibility = View.GONE
                btResp3.visibility = View.GONE
                btResp4.visibility = View.GONE
            }
        }

        if (pergunta.tipo == "boolean") {
            if (pergunta.resposta_certa == "true") {
                btResp.text = pergunta.resposta_certa
                btResp2.text = pergunta.respostas_incorretas[0]
            } else {
                btResp.text = pergunta.respostas_incorretas[0]
                btResp2.text = pergunta.resposta_certa

            }
            btResp3.visibility = View.GONE
            btResp4.visibility = View.GONE
        }

        btResp.setOnClickListener {
            marcarpontuaçãoSimple(pergunta, btResp.text as String);
        }
        btResp2.setOnClickListener {
            marcarpontuaçãoSimple(pergunta, btResp2.text as String);
        }
        btResp3.setOnClickListener {
            marcarpontuaçãoSimple(pergunta, btResp3.text as String);
        }
        btResp4.setOnClickListener {
            marcarpontuaçãoSimple(pergunta, btResp4.text as String);
        }



        btRemote.setOnClickListener {
            val intentExplicita = Intent(this, CategoriasActivity::class.java)

            intentExplicita.putExtra("dificuldade", pergunta.dificuldade)
            intentExplicita.putExtra("Categoria", pergunta.categoria)
            startActivity(intentExplicita)
        }
    }

    @SuppressLint("NewApi")
    private fun loadQuest(pergunta: List<Pergunta>) {

        var tempo: Int
        tempo = 1
        if (dificuldade == "easy")
            tempo = 45000

        if (dificuldade == "medium")
            tempo = 30000

        if (dificuldade == "hard")
            tempo = 15000

        txtTime.max = tempo / 1000

        timer = object : CountDownTimer(tempo.toLong(), 1000) {

            override fun onTick(millisUntilFinished: Long) {
                txtTime.progress = (millisUntilFinished / 1000).toInt()

            }

            override fun onFinish() {
                timerFinished = true
                val toast = Toast.makeText(this@JogoActivity, "Acabou o tempo", Toast.LENGTH_SHORT)
                toast.show()
                btLocal.visibility = View.VISIBLE
                btRemote.visibility = View.VISIBLE

            }
        }.start()

        if (Build.VERSION.SDK_INT >= 24)
            txtQuestao.text = Html.fromHtml(pergunta[0].questao, Html.FROM_HTML_MODE_LEGACY).toString()
        else
            txtQuestao.text = Html.fromHtml(pergunta[0].questao).toString()

        if (pergunta[0].tipo == "multiple") {
            val random = Random()

            var lista = pergunta[0].respostas_incorretas
            lista += pergunta[0].resposta_certa

            lista.shuffled()

            if (lista.lastIndex == 5) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.text = lista[3]
            } else if (lista.lastIndex == 4) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.text = lista[3]
            } else if (lista.lastIndex == 3) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.text = lista[3]
            } else if (lista.lastIndex == 2) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.text = lista[2]
                btResp4.visibility = View.GONE
            } else if (lista.lastIndex == 1) {
                btResp.text = lista[0]
                btResp2.text = lista[1]
                btResp3.visibility = View.GONE
                btResp4.visibility = View.GONE
            } else {
                btResp.text = lista[0]
                btResp2.visibility = View.GONE
                btResp3.visibility = View.GONE
                btResp4.visibility = View.GONE
            }
        }

        if (pergunta[0].tipo == "boolean") {
            if (pergunta[0].resposta_certa == "true") {
                btResp.text = pergunta[0].resposta_certa
                btResp2.text = pergunta[0].respostas_incorretas[0]
            } else {
                btResp.text = pergunta[0].respostas_incorretas[0]
                btResp2.text = pergunta[0].resposta_certa

            }
            btResp3.visibility = View.GONE
            btResp4.visibility = View.GONE
        }

        btResp.setOnClickListener {
            marcarpontuação(pergunta, btResp.text as String);
        }
        btResp2.setOnClickListener {
            marcarpontuação(pergunta, btResp2.text as String);
        }
        btResp3.setOnClickListener {
            marcarpontuação(pergunta, btResp3.text as String);
        }
        btResp4.setOnClickListener {
            marcarpontuação(pergunta, btResp4.text as String);
        }

        btPerguntaTardia.setOnClickListener {
            perguntaDao.inserir(pergunta[0])
            marcarpontuaçãotardia()

            btPerguntaTardia.visibility = View.GONE
            btResp.visibility = View.GONE
            btResp2.visibility = View.GONE
            btResp3.visibility = View.GONE
            btResp4.visibility = View.GONE
            btLocal.visibility = View.VISIBLE
            btRemote.visibility = View.VISIBLE
            timer.cancel()
        }

        btRemote.setOnClickListener {
            val intentExplicita = Intent(this, CategoriasActivity::class.java)
            intentExplicita.putExtra("dificuldade", dificuldade)
            intentExplicita.putExtra("Categoria", b)
            startActivity(intentExplicita)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (::timer.isInitialized && !timerFinished)
            timer.cancel()
    }

    fun marcarpontuaçãoSimple(pergunta: Pergunta, resposta: String) {
        var pontuacao = 0
        if (pergunta.dificuldade == "easy")
            pontuacao = 4
        if (pergunta.dificuldade == "medium")
            pontuacao = 6
        if (pergunta.dificuldade == "hard")
            pontuacao = 8

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)

        var senha = prefs.getString("senha", "")
        var email = prefs.getString("email", "")

        btPerguntaTardia.visibility = View.GONE

        if (resposta == pergunta.resposta_certa) {
            serviceUsuario.marcarPontuacao(email, senha, pontuacao).enqueue(object : Callback<RetornoPontuacao> {
                override fun onFailure(call: Call<RetornoPontuacao>, t: Throwable) {
                    val toast = Toast.makeText(this@JogoActivity, "algo deu errado :(", Toast.LENGTH_SHORT)
                    toast.show()
                }

                override fun onResponse(call: Call<RetornoPontuacao>, response: Response<RetornoPontuacao>) {
                    val toast = Toast.makeText(this@JogoActivity, "Correct", Toast.LENGTH_SHORT)
                    toast.show()

                    btLocal.visibility = View.VISIBLE
                    btRemote.visibility = View.VISIBLE
                    perguntaDao.apagar(pergunta)
                }
            })

        } else {
            serviceUsuario.marcarPontuacao(email, senha, pontuacao * -1).enqueue(object : Callback<RetornoPontuacao> {
                override fun onFailure(call: Call<RetornoPontuacao>, t: Throwable) {
                    val toast = Toast.makeText(this@JogoActivity, "algo deu errado :(", Toast.LENGTH_SHORT)
                    toast.show()
                }

                override fun onResponse(call: Call<RetornoPontuacao>, response: Response<RetornoPontuacao>) {
                    val toast = Toast.makeText(this@JogoActivity, "Wrong", Toast.LENGTH_SHORT)
                    toast.show()
                    btLocal.visibility = View.VISIBLE
                    btRemote.visibility = View.VISIBLE
                    perguntaDao.apagar(pergunta)
                }
            })
        }

    }


    fun marcarpontuação(pergunta: List<Pergunta>, resposta: String) {
        var pontuacao = 0
        if (dificuldade == "easy")
            pontuacao = 5
        if (dificuldade == "medium")
            pontuacao = 8
        if (dificuldade == "hard")
            pontuacao = 10

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)

        var senha = prefs.getString("senha", "")
        var email = prefs.getString("email", "")

        btPerguntaTardia.visibility = View.GONE

        if (resposta == pergunta[0].resposta_certa) {
            serviceUsuario.marcarPontuacao(email, senha, pontuacao).enqueue(object : Callback<RetornoPontuacao> {
                override fun onFailure(call: Call<RetornoPontuacao>, t: Throwable) {
                    val toast = Toast.makeText(this@JogoActivity, "algo deu errado :(", Toast.LENGTH_SHORT)
                    toast.show()
                }

                override fun onResponse(call: Call<RetornoPontuacao>, response: Response<RetornoPontuacao>) {
                    val toast = Toast.makeText(this@JogoActivity, "Correct", Toast.LENGTH_SHORT)
                    toast.show()
                    btLocal.visibility = View.VISIBLE
                    btRemote.visibility = View.VISIBLE
                }
            })

        } else {
            serviceUsuario.marcarPontuacao(email, senha, pontuacao * -1).enqueue(object : Callback<RetornoPontuacao> {
                override fun onFailure(call: Call<RetornoPontuacao>, t: Throwable) {
                    val toast = Toast.makeText(this@JogoActivity, "algo deu errado :(", Toast.LENGTH_SHORT)
                    toast.show()
                }

                override fun onResponse(call: Call<RetornoPontuacao>, response: Response<RetornoPontuacao>) {
                    val toast = Toast.makeText(this@JogoActivity, "Wrong", Toast.LENGTH_SHORT)
                    toast.show()
                    btLocal.visibility = View.VISIBLE
                    btRemote.visibility = View.VISIBLE
                }
            })
        }

        timer.cancel()
    }

    fun marcarpontuaçãotardia() {
        var pontuacao = 0

        if (dificuldade == "easy")
            pontuacao = -2
        if (dificuldade == "medium")
            pontuacao = -4
        if (dificuldade == "hard")
            pontuacao = -6

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)

        var senha = prefs.getString("senha", "")
        var email = prefs.getString("email", "")

        serviceUsuario.marcarPontuacao(email, senha, pontuacao).enqueue(object : Callback<RetornoPontuacao> {
            override fun onFailure(call: Call<RetornoPontuacao>, t: Throwable) {
                val toast = Toast.makeText(this@JogoActivity, "algo deu errado :(", Toast.LENGTH_SHORT)
                toast.show()
            }

            override fun onResponse(call: Call<RetornoPontuacao>, response: Response<RetornoPontuacao>) {
                val toast = Toast.makeText(this@JogoActivity, "Perdeu alguns pontos por isso", Toast.LENGTH_SHORT)
                toast.show()
            }
        })
    }

}