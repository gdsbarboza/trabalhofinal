package br.edu.ifpr.gustavo.trabalhofinal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import br.edu.ifpr.gustavo.trabalhofinal.bd.AppDatabase
import br.edu.ifpr.gustavo.trabalhofinal.bd.ResultadoDao
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Categoria
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Pergunta
import br.edu.ifpr.gustavo.trabalhofinal.ui.CategoriaListListener
import br.edu.ifpr.gustavo.trabalhofinal.ui.QuestaoAdapter
import br.edu.ifpr.gustavo.trabalhofinal.ui.QuestaoListListener
import kotlinx.android.synthetic.main.activity_local.*
import retrofit2.Retrofit

class LocalActivity : AppCompatActivity(), QuestaoListListener {
    override fun abrirActivity(pergunta: Pergunta) {
        val intentExplicita = Intent(this, JogoActivity::class.java)
//        intentExplicita.putExtra("dificuldade", dificuldade)
        intentExplicita.putExtra("Pergunta", pergunta)
//        intentExplicita.putExtra("Categoria", b)
        startActivity(intentExplicita)
    }

    lateinit var perguntaDao: ResultadoDao
    lateinit var db: AppDatabase
    lateinit var dificuldade: String
    lateinit var b: Categoria
    lateinit var adapter: QuestaoAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local)

        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "perguntas.db"
        )
            .allowMainThreadQueries()
            .addMigrations()
            .build()
        perguntaDao = db.resultadoDao()

       var list = perguntaDao.buscaTodas()
        loadRecyclerView(list)


    }
    private fun loadRecyclerView(resultado: List<Pergunta>) {
        if (listQuestaoLocal != null) {
            val mRecycler = this.findViewById(R.id.listQuestaoLocal) as RecyclerView
            mRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
            adapter = QuestaoAdapter(resultado, this)
            mRecycler.adapter = adapter
        }
    }

}
