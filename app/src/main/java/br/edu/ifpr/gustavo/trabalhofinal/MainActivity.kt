package br.edu.ifpr.gustavo.trabalhofinal

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifpr.gustavo.trabalhofinal.bd.AppDatabase
import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoLogin
import br.edu.ifpr.gustavo.trabalhofinal.service.UsuarioService
import kotlinx.android.synthetic.main.activity_logar.*
import kotlinx.android.synthetic.main.activity_logar.txtEmail
import kotlinx.android.synthetic.main.activity_logar.txtSenha
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.widget.Toast

class MainActivity : AppCompatActivity() {
	lateinit var db: AppDatabase
	lateinit var retrofit: Retrofit
	lateinit var service: UsuarioService
	lateinit var prefs: SharedPreferences

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)

		setContentView(R.layout.activity_logar)

		if(prefs.contains("email") && prefs.contains("senha")){
			criaActivity()
		}

		configuraRetrofit()
		activityLogar()
	}

	fun criaActivity(){
		val intentGenerica = Intent(this, InicialActivity::class.java)
		startActivity(intentGenerica)
	}

	fun mensagemToast(mensagem: String){
		val toast = Toast.makeText(this,mensagem, Toast.LENGTH_SHORT)
		toast.show()
	}

	fun configuraRetrofit() {
		retrofit = Retrofit.Builder()
				.baseUrl("https://tads2019-todo-list.herokuapp.com/")
				.addConverterFactory(GsonConverterFactory.create())
				.build()
		service = retrofit.create(UsuarioService::class.java)
	}

	fun activityLogar(){
		btLogar.setOnClickListener{

			//            btLogar.disable
			val email = txtEmail.text.toString()
			val senha = txtSenha.text.toString()

			service.fazerLogin(email,senha).enqueue(object : Callback<RetornoLogin> {
				override fun onFailure(call: Call<RetornoLogin>, t: Throwable) {
				}

				override fun onResponse(call: Call<RetornoLogin>, response: Response<RetornoLogin>) {
					val r = response.body()!!

					if(r.sucesso) {
						prefs.edit().putString("email", email).commit()
						prefs.edit().putString("senha", senha).commit()
						mensagemToast(r.mensagem)

						criaActivity()
					}else{
						mensagemToast("Login Incorreto!")
					}
				}

			})

		}

		txtCadastro.setOnClickListener {
			val intentExplicita = Intent(this, CadastroActivity::class.java)
			startActivity(intentExplicita)
		}
	}

}

