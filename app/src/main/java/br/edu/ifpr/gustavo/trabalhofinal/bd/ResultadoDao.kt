package br.edu.ifpr.gustavo.trabalhofinal.bd

import androidx.room.*
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Pergunta


@Dao
interface ResultadoDao {
    @Query("SELECT * FROM perguntas")
    fun buscaTodas(): List<Pergunta>

    @Insert
    fun inserir(pergunta: Pergunta): Long

    @Update
    fun atualizar(pergunta: Pergunta)

    @Delete
    fun apagar(pergunta: Pergunta)

//    @Insert
//    fun inserirRespostas(respostas: List<RetornoPergunta>)
}
