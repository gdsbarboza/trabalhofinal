package br.edu.ifpr.gustavo.trabalhofinal.entidades

import androidx.room.Entity

@Entity(tableName = "pontuacao")
data class RetornoPontuacao (
    var sucesso: Boolean,
    var mensagem: String,
    var pontuacao: Int
)

