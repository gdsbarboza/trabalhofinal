package br.edu.ifpr.gustavo.trabalhofinal.ui

import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpr.gustavo.trabalhofinal.R
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Pergunta
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Usuario
import kotlinx.android.synthetic.main.card_questao.view.*
import kotlinx.android.synthetic.main.card_ranking.view.*
import java.text.SimpleDateFormat

class RankingAdapter(var resultado: List<Usuario>) : RecyclerView.Adapter<RankingAdapter.RankingViewHolder>() {
    override fun getItemCount() = resultado.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RankingViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.card_ranking, parent, false)
        )

    override fun onBindViewHolder(holder: RankingViewHolder, position: Int) {
        holder.fillUI(resultado[position])
    }

    inner class RankingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fillUI(usuario: Usuario) {


            itemView.userName.text = usuario.nome
            itemView.Pontuation.text = usuario.pontuacao.toString()
            itemView.timesPlayed.text = usuario.partidasJogadas.toString()
            val f = SimpleDateFormat("dd/MM/yyyy hh:mm")
            itemView.txtData.text = f.format(usuario.ultimaPartida)

        }
    }

}

