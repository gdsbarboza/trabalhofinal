package br.edu.ifpr.gustavo.trabalhofinal.entidades

import androidx.room.Entity

@Entity(tableName = "logins")

data class RetornoLogin (
    var sucesso: Boolean,
    var mensagem: String,
    var pontuacao: Int

)
