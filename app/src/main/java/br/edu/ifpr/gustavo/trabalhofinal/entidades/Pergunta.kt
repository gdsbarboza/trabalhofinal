package br.edu.ifpr.gustavo.trabalhofinal.entidades
import androidx.room.*
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@TypeConverters(Converters::class)
@Entity(tableName = "perguntas")
data class Pergunta (
    @SerializedName("category")
    var categoria: String,
    @SerializedName("type")
    var tipo: String,
    @SerializedName("difficulty")
    var dificuldade: String,
    @SerializedName("question")
    var questao: String,
    @SerializedName("correct_answer")
    var resposta_certa: String,
    @SerializedName("incorrect_answers")
    var respostas_incorretas: List<String>

//json type convert ouu criiar dois do pergunta, um pra local e outro pra remoto

) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}