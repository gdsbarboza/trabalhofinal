package br.edu.ifpr.gustavo.trabalhofinal.ui

import br.edu.ifpr.gustavo.trabalhofinal.entidades.Categoria

interface CategoriaListListener {
    fun abrirActivity(categoria: Categoria, dificuldade: String)
}