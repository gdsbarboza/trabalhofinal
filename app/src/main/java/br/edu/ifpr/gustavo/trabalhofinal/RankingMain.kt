package br.edu.ifpr.gustavo.trabalhofinal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoRanking
import br.edu.ifpr.gustavo.trabalhofinal.entidades.Usuario
import br.edu.ifpr.gustavo.trabalhofinal.service.UsuarioService
import br.edu.ifpr.gustavo.trabalhofinal.ui.RankingAdapter
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_ranking_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RankingMain : AppCompatActivity() {

    lateinit var retrofit: Retrofit
    lateinit var service: UsuarioService
    lateinit var adapter: RankingAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ranking_main)
        configuraRetrofit()

        service.mostrarRanking().enqueue(object : Callback<RetornoRanking> {
            override fun onFailure(call: Call<RetornoRanking>, t: Throwable) {
                Log.e("DEUEERO", t.message, t)
            }

            override fun onResponse(call: Call<RetornoRanking>, response: Response<RetornoRanking>) {
                val r = response.body()!!


                loadRecyclerView(r.ranking)
            }
        })
    }

    fun configuraRetrofit() {
        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T00;'HH:mm.ssZ").create()
        retrofit = Retrofit.Builder()
            .baseUrl("https://tads2019-todo-list.herokuapp.com")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        service = retrofit.create(UsuarioService::class.java)
    }

    fun loadRecyclerView(usuarios: List<Usuario>) {
        if (listPessoaRanking != null) {
            val mRecycler = this.findViewById(R.id.listPessoaRanking) as RecyclerView
            mRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            adapter = RankingAdapter(usuarios)
            mRecycler.adapter = adapter
        }
    }
}