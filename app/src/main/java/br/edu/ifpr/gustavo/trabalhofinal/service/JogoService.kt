package br.edu.ifpr.gustavo.trabalhofinal.service

import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoPergunta
import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoCategoria
import retrofit2.Call
import retrofit2.http.*

interface JogoService {
    @Headers("Accept: application/json")
    @GET("api.php")
    fun coletaQuestao(

        @Query("amount")
        contagem: Int,

        @Query("category")
        categoria: Int,

        @Query("difficulty")
        dificuldade: String

    ): Call<RetornoPergunta>

    @Headers("Accept: application/json")
    @GET("api_category.php")
    fun coletaCategoria(): Call<RetornoCategoria>


}