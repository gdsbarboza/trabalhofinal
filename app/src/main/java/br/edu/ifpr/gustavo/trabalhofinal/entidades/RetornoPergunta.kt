package br.edu.ifpr.gustavo.trabalhofinal.entidades

import com.google.gson.annotations.SerializedName

data class RetornoPergunta (
    @SerializedName("response_cod")
    var totalResults: Int,
    @SerializedName("results")
    var perguntas: List<Pergunta>
)