package br.edu.ifpr.gustavo.trabalhofinal

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.edu.ifpr.gustavo.trabalhofinal.entidades.RetornoCriarUsuario
import br.edu.ifpr.gustavo.trabalhofinal.service.UsuarioService
import kotlinx.android.synthetic.main.activity_cadastrar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CadastroActivity : AppCompatActivity() {
    lateinit var retrofit: Retrofit
    lateinit var service: UsuarioService
    lateinit var email: String
    lateinit var senha: String
    lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastrar)
        configuraRetrofit()

        btCadastro.setOnClickListener {
            carregaDados()
        }

    }

    fun configuraRetrofit() {
        retrofit = Retrofit.Builder()
            .baseUrl("https://tads2019-todo-list.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        service = retrofit.create(UsuarioService::class.java)
    }

    fun carregaDados() {
        val nome = txtNome.text.toString()
        email = txtEmail.text.toString()
        senha = txtSenha.text.toString()
        val confiSenha = txtConfiSenha.text.toString()

        if(senha == confiSenha){

            service.criarUsuario(nome,email,senha).enqueue(object : Callback<RetornoCriarUsuario>{
                override fun onFailure(call: Call<RetornoCriarUsuario>, t: Throwable) {
                }

                override fun onResponse(call: Call<RetornoCriarUsuario>, response: Response<RetornoCriarUsuario>) {
                    val r = response.body()!!
                    if(r.sucesso)
                        prefs.edit().putString("email", email).commit()
                        prefs.edit().putString("senha", senha).commit()
                        criaActivity()
                }

            })

        }
    }

    fun criaActivity(){
        val intentGenerica = Intent(this, CategoriasActivity::class.java)

        prefs = getPreferences(Context.MODE_PRIVATE)
        prefs.edit().putString("email", email).commit()
        prefs.edit().putString("senha", senha).commit()

        startActivity(intentGenerica)
    }

}
